# Documentação SDL_net #

Respositório contendo a tradução para Português Brasileiro da página da documentação do 
SDL_net.

Originalmente disponível em:

[https://www.libsdl.org/projects/SDL_net/docs/SDL_net_frame.html](https://www.libsdl.org/projects/SDL_net/docs/SDL_net_frame.html)